import React from 'react'
import { useTheme } from '@mui/material/styles';
import { Grid, Link, useMediaQuery } from '@mui/material'
import { Box } from '@mui/system'
import './Footer.css'

const links = [
    'Home',
    'Terms and Conditions',
    'Privacy Policy',
    'Collection Statement',
    'Help',
    'Manage Account'
]

export default function Footer() {
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('sm'));

    return (
        <Grid container mt={10} sx={{
            backgroundColor: 'black',
            width: "100%",
            position: 'static',
            bottom: 0
        }} justifyContent={'center'}>
            <Grid item xs={10}>
                {links.map(link => (
                    <Box m={1}>
                        <Link>{link}</Link>
                    </Box>
                ))}
            </Grid>
            Footer
        </Grid>
    )
}
