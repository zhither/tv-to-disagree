import React, { useEffect, useState } from 'react'
import { Box, Card, CardActionArea, CardMedia, Divider, Grid, Popover, Typography } from '@mui/material'
import { useSelector } from 'react-redux';
import background from '../../assets/placeholder.png'

export default function Movies(movies) {
    const [chosenMovie, setChosenMovie] = useState('')
    const state = useSelector((state) => state.data)
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event, movie) => {
        setAnchorEl(event.currentTarget);
        setChosenMovie(movie)
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return (
        <>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
            // sx={{maxWidth: 800}}
            >
                {
                    chosenMovie &&
                    <Box display={'flex'}>
                        <img width="30%" height="30%" src={chosenMovie?.images['Poster Art']?.url || background} />
                        <Box>
                            <Typography variant='h4' sx={{ p: 2 }}>{chosenMovie.title}</Typography>
                            <Typography sx={{ p: 2 }}>{chosenMovie.releaseYear}</Typography>
                            <Divider />
                            <Typography sx={{ p: 2 }}>{chosenMovie.description}</Typography>
                        </Box>
                    </Box>
                }
            </Popover>
            <Grid container
                rowSpacing={2}
                columnSpacing={2}
                sx={{
                    marginLeft: "auto",
                    marginRight: "auto",
                    maxWidth: '90%'
                }}>
                {
                    state.movies ?
                        state.movies?.map((movie) => (
                            <Grid item xs={12} lg={4}>
                                <Card>
                                    <CardActionArea onClick={(event) => handleClick(event, movie)}>
                                        <CardMedia
                                            component="img"
                                            height="800"

                                            image={movie.images['Poster Art'].url}
                                            alt="movie pic"
                                        >
                                        </CardMedia>
                                        <Typography variant="h3" component="div">
                                            {movie.title}
                                        </Typography>
                                    </CardActionArea>
                                </Card>
                            </Grid>
                        ))
                        :
                        <div> error al cargar las peliculas</div>
                }
            </Grid>
        </>
    )
}
