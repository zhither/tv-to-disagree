import React, { useEffect } from 'react'
import { Card, CardActionArea, CardMedia, Grid, Typography } from '@mui/material'
import background from '../../assets/placeholder.png'
import { Link } from "react-router-dom";

export default function Home() {

    return (
        <>
            <Grid container justifyContent={'center'} rowSpacing={10}>
                <Grid item xs={11} >
                    <Typography gutterBottom variant="h1" component="div">
                        Welcome to Tv to Disagree
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Pellentesque ut eleifend dolor. Nunc tincidunt lorem ac rutrum tempus.
                        Phasellus ut aliquam eros. Nam venenatis quis nulla non efficitur.
                        Phasellus posuere eleifend ex vestibulum molestie. Nunc in risus lectus.
                        Proin purus mauris, gravida sollicitudin vulputate non, sagittis quis purus.
                        Aenean vel tortor ac neque vulputate aliquam dapibus id dui. Quisque vitae mauris arcu.
                        Maecenas ipsum quam, euismod id tortor eget, congue venenatis justo.
                    </Typography>

                </Grid>
                <Grid item xs={7} sm={5}>
                    <Link to='/movies'>
                        <Card sx={{ maxWidth: 345, backgroundColor: '#29434e' }}>
                            <CardActionArea>
                                <CardMedia
                                    sx={{ color: 'black' }}
                                    component="img"
                                    height="140"
                                    image={background}
                                    alt="movie pic"
                                >
                                </CardMedia>
                                <Typography gutterBottom variant="h4" component="div">
                                    movies
                                </Typography>
                            </CardActionArea>
                        </Card>
                    </Link>
                </Grid>
                <Grid item xs={7} sm={5}>
                    <Link to='/series'>
                        <Card sx={{ maxWidth: 345, backgroundColor: '#29434e' }}>
                            <CardActionArea>
                                <CardMedia
                                    sx={{ color: 'black' }}
                                    component="img"
                                    height="140"
                                    image={background}
                                    alt="movie pic"
                                >

                                </CardMedia>
                                <Typography gutterBottom variant="h4" component="div">
                                    series
                                </Typography>
                            </CardActionArea>
                        </Card>
                    </Link>
                </Grid>
            </Grid>
        </>
    )
}
