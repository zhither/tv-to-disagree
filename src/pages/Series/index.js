import React, { useEffect, useState } from 'react'
import { Box, Card, CardActionArea, CardMedia, Divider, Grid, Popover, Typography } from '@mui/material'
import { useSelector } from 'react-redux';

export default function Series(serie) {
  const [chosenSerie, setChosenSerie] = useState('')
  const state = useSelector((state) => state.data)
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event, serie) => {
    setAnchorEl(event.currentTarget);
    setChosenSerie(serie)
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        {
          chosenSerie &&
          <Box display={'flex'}>
            <img width="30%" height="30%" src={chosenSerie.images['Poster Art'].url} />
            <Box>
              <Typography variant='h4' sx={{ p: 2 }}>{chosenSerie.title}</Typography>
              <Typography sx={{ p: 2 }}>{chosenSerie.releaseYear}</Typography>
              <Divider />
              <Typography sx={{ p: 2 }}>{chosenSerie.description}</Typography>
            </Box>
          </Box>
        }
      </Popover>
      <Grid container
        rowSpacing={2}
        columnSpacing={2}
        sx={{
          marginLeft: "auto",
          marginRight: "auto",
          maxWidth: '90%'
        }}>
        {
          state.series ?
            state.series?.map((serie) => (
              <Grid item xs={12} lg={4}>
                <Card Paper >
                  <CardActionArea onClick={(event) => handleClick(event, serie)}>
                    <CardMedia
                      component="img"
                      height="800"

                      image={serie.images['Poster Art'].url}
                      alt="serie pic"
                    >
                    </CardMedia>
                    <Typography variant="h3" component="div">
                      {serie.title}
                    </Typography>
                  </CardActionArea>
                </Card>
              </Grid>
            ))
            :
            <div> error al cargar las series</div>
        }
      </Grid>
    </>
  )
}
