import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { fetchApi } from './dataApi';

const initialState = {
    total: 100,
    loading: false,
    movies: [],
    series: [],
    entries: []
};

export const getMoviesAndTv = createAsyncThunk(
    'data/fetchApi',
    async () => {
        const response = await fetchApi();
        // The value we return becomes the `fulfilled` action payload
        return response.data;
    }
);

export const dataSlice = createSlice({
    name: 'data',
    initialState,
    reducers: {
        loading: (state) => {
            state.loading = true;
        },
    },
    extraReducers: (builder) => {
        // Add reducers for additional action types here, and handle loading state as needed
        builder.addCase(getMoviesAndTv.fulfilled, (state, action) => {
            let movies = action.payload
                .filter(movie => movie.programType === 'movie' && movie.releaseYear >= 2010)
                .sort( (a, b) =>  a.title < b.title ? -1 : 1)
            let series = action.payload
                .filter(serie => serie.programType === 'series' && serie.releaseYear >= 2010)
                .sort( (a, b) =>  a.title < b.title ? -1 : 1)
            return {
                ...state,
                movies,
                series,
                loading: false,
                entries: action.payload,
            }
        })
    },
});

export const { movieList, loading } = dataSlice.actions;


export default dataSlice.reducer;
