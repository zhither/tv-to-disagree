// A mock function to mimic making an async request for data
import data from '../../data/sample.json'

export function fetchApi() {
    return new Promise((resolve) =>
        setTimeout(() =>
            resolve({ data: data.entries }), 1000)
    );
}