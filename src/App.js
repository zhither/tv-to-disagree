import React, { useEffect } from 'react';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Movies from './pages/Movies';
import Series from './pages/Series';
import Home from './pages/Home';
import { AppBar, Backdrop, Button, CircularProgress, IconButton, Toolbar, Typography } from '@mui/material';
import { Box } from '@mui/system';
import MenuIcon from '@mui/icons-material/Menu';
import Footer from './Components/Footer';
import { useDispatch, useSelector } from 'react-redux';
import { getMoviesAndTv, loading } from './features/data/dataSlice';

function App() {

  const state = useSelector((state) => state.data)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(loading())
    dispatch(getMoviesAndTv())
  }, []);
  
  return (
    <>
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={state.entries.length < 1}
      >
        <Box sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center'
        }}>
          <CircularProgress color="inherit" />
          <Typography>
            Cargando
          </Typography>
        </Box>
        <Box>
        </Box>
      </Backdrop>
      <Box sx={{ marginBottom: '30px' }}>
        <AppBar position="static" sx={{ backgroundColor: '#546E7A' }}>
          <Toolbar>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              we love movies and tv shows
            </Typography>
            <Button color="inherit">Login</Button>
          </Toolbar>
        </AppBar>
      </Box>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/Movies" element={<Movies movies={state.movies} />} />
          <Route path="/series" element={<Series series={state.series} />} />
        </Routes>
      </BrowserRouter>
      <Footer />
    </>
  );
}

export default App;
