## prueba

# Getting Started with Create React App and Redux

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## fin prueba

<img src="assets/logo.png" alt="drawing" width="200"/>

# Prueba técnica — React developer

## Objetivo

El objetivo de esta prueba técnica es que el candidato muestre sus habilidades con las herramientas que utilizará luego en su trabajo diario con nuestro equipo. Está diseñado para verificar las habilidades de desarrollo front-end utilizando React y su capacidad para resolver problemas.

Pondremos el foco en obtener un **código simple, bien diseñado y organizado, eficaz y testeado**, así como el cumplimiento de todos los requerimientos solicitados.

## Desarrollo del proyecto

- Se deberá clonar este repositorio para poder modificarlo y completarlo con la resolución del proyecto.
- Una vez que su código esté listo, suba el código a un repositorio público propio y envíenos el enlace a dicho repositorio para su revision.

> Se pueden utilizar herramientas como [create-react-app](https://github.com/facebookincubator/create-react-app) y similares para inicializar el proyecto.

## Prueba técnica

Usando la estructura vista en las imágenes proporcionadas como referencia, deberá crear un conjunto de pantallas y componentes React para crear la aplicación solicitada.

Se deberá incluir también `README` con instrucciones de configuración/ejecución y cualquier prueba u otra documentación que haya creado como parte de su solución.

Además, agregue la siguiente información a su archivo `README`:

- ¿Cómo decidió las opciones técnicas y arquitectónicas utilizadas como parte de su solución?

  Utilice la nueva version de redux implementando las mejroes practicas para manjear el estado, mas alla de eso utilice MUI como liberia para los componentes ya que es rapida y sencilla

- ¿Hay alguna mejora que pueda hacer en su envío?

  si, es algo muy rapido y sencillo, tiene muchos puntos de mejoras en el UI/UX principalmente, trate de enfocarme en el manejo de estado y manejo de arrays ya que fue lo que considere mas importante para el challenge

- ¿Qué haría de manera diferente si se le asignara más tiempo?

  me enfocaria en mejorar la UI, es algo muy importante, dedicarle tiempo para diseñarlo, agregar animaciones y una paleta de colores mas llamativa. Por otro lado mejoraria el routing, añadiria rutas seguras y publicas

- ¿El método setState() es asíncrono?

  si, porque las acciones realizadas dentro de este metodo pueden ser agrupadas y atendidas en stack

- ¿Qué configuraciones admite setState()?
  un objeto que cambia el valor actual del state, estado actual y los porps actuales o un objeto que cambia el valor del state y un callback

- ¿Qué es el DOM virtual?
  es la representacion IU que se mantiene en memoria y en sincronía con el DOM real

- ¿Qué son las referencias en React?
  ES una manera de acceder a un nodo del DOM creados en el método de renderizado.

- ¿Por qué es inviable retornar elementos adyacentes en JSX?
  porque los elementos deben partir de un único elemento superior.

- ¿React es una biblioteca o un framework?
  biblioteca

- ¿Qué son los fragmentos?
  es un componente para agrupar elementos sin introducir elementos extra a nivel de DOM.

- ¿Qué función cumplen las keys en React?
  permite saber al virtualDOM que elementos tiene que actualizar

- ¿Qué objetivo cumple ReactDOM?
  no manipular directamente el DOM a menos que haya alguna diferencia entre la copia (virtualDOM) y el real

- ¿Qué es el diseño atómico?
  es una metodología que consiste en que la UI se agrepe en un pequeño grupo de componentes atómicos y una serie de reglas para agruparlos en componentes cada vez más complejos

- ¿Qué son los componentes controlados?
  son componentes cuyo estado es manejado directamente por el desarrollador
- ¿Cómo limitar el alcance de los estilos en React?

- ¿Qué función cumple shouldComponentUpdate()?
  controlar la actualizacion del componente

- ¿Qué función cumple el componente StrictMode?
  identificar problemas potenciales en la aplicación.

- ¿Deben favorecerse los componentes funcionales frente a los de estado?
  Segun react, desde la entrada de los Hooks se tiene que favorecer los componentes funcionales ya que apuntan a que la libreria se manaje de forma funcional

- ¿Qué diferencias existen entre el montaje y el renderizado de un componente?
  Montaje: React monta cada componente cuando este se muestra en la interfaz por primera vez
  
  Renderizado: React renderiza un componente la primera vez que se muestra, y luego en respuesta a los cambios en sus props o estado

## Detalles

Necesitará construir las siguientes 3 páginas con React:

- Una página de "Inicio"
- Una página de "Series"
- Una página "Películas"

Cree componentes para cada parte de la página (por ejemplo, encabezado, contenido, pie de página, etc.). Dentro de la carpeta `/assets` podrá encontrar distintas imágenes para utilizar.

Las páginas también deben poder utilizarse en dispositivos móviles.

Puede suponer que no tiene que admitir navegadores heredados sin funciones como `fetch` o `flexbox`.

### Página de “Inicio”

> Ejemplo de referencia [screens/1-home.jpg](./screens/1-home.jpg).

Esta será su pantalla index.html.

Deberá mostrar 2 bloques que conectarán con las páginas de "Series" y "Películas".

### Páginas de “Serie” y “Películas”

> Ejemplo de referencia [screens/2-series.jpg](./screens/2-series.jpg) y [screens/3-movies.jpg](./screens/3-movies.jpg).

Para cada página debería leer los datos desde el archivo JSON [feed/sample.json](https://raw.githubusercontent.com/StreamCo/react-coding-challenge/master/feed/sample.json), luego:

- Mostrar los primeros 20 resultados (`entries`). No es necesario paginar para ver más resultados.
- Mostrar sólo si contienen el atributo `releaseYear` >= `2010`
- Ordenar los resultados por el atributo `title` de manera ascendente con órden alfanumérico
- Para la página de "Series" usar resultados con `programType` igual a `series`.
- Para la página de "Películas" usar resultados con `programType` igual a `movie`.
- Los atributos que debes utilizar para mostrar en la caja de cada resultado son:
  - `title`
  - `images` → `Poster Art` → `url`
- Al posicionar el mouse sobre cada resultado la caja debe reducir su opacidad y mostrar borde blanco.
- Al hacer click sobre el titulo deberá abrirse un popup mostrando la información completa:
  - `title`
  - `description`
  - `releaseYear`
  - `images` → `Poster Art` → `url`

### Otras consideraciones

También necesitará manejar los estados de carga/loading y error de obtener los datos desde el archivo JSON:

- Estado de "Carga/Loading" [screens/1.1-loading.jpg](./screens/1.1-loading.jpg)
- Estado de "Error" [screens/1.2-error.jpg](./screens/1.2-error.jpg)

#### Opcional

- Filtro por año
  - agregar arriba del listado de series/películas un input que permita filtrar películas por año.
- Paginación
  - agregar un selector de cantidad de resultados a mostrar (5, 10, 20)
  - permitir ir a próxima página de resultados o página anterior
  - permitir moverse de página en página utilizando un parámetro en la URL

## Requisitos de Stack

Para el desarrollo de la aplicación deberá utilizar:

- React / React Hooks
- Redux
- Librería de estilos (styled-components, CSS modules, o similar)
- Mobile friendly
- Unit tests (jest, react-testing-library, o similar)
- Manejo de errores
- _(opcional)_ TypeScript
- _(opcional)_ Integration/E2E tests
- _(opcional)_ Deploy automático
- _(opcional)_ ...

Importante saber:

- No es necesario crear un entorno de desarrollo/producción.
- Se pueden utilizar otras librerías que crea conveniente, aunque se recomienda proporcionar una solución básica ajustada a lo solicitado, ya que nuestro objetivo principal es evaluar sus habilidades con React y Javascript.
- Como empresa, creemos que la comunicación es la clave del éxito. Entonces, si algo no está claro, o si tiene dudas sobre la tarea, consultanos!
- El servicio a consultar se encuentra en el api de: [The Movie Database API](https://developers.themoviedb.org)
- Considere crear las cuentas y suministrar las credenciales que considere necesaria para la conexion a dicho servicio

> Happy coding!

<img src="https://user-images.githubusercontent.com/5693916/30273942-84252588-96fb-11e7-9420-5516b92cb1f7.gif" width="100">\_
